module.exports = module.exports = function babelConfig(api) {
  api.cache(true)
  return {
    presets: [
      [
        '@babel/preset-env',
        {
          targets: {
            node: 'current'
          }
        }
      ]
    ],
    plugins: ['@babel/plugin-proposal-class-properties'],
    env: {
      test: {
        presets: ['@babel/preset-env']
      }
    }
  }
}
