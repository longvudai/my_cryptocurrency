import Block from '../src/blockchain/Block'

describe('Block', () => {
  let data, lastBlock, block

  beforeEach(() => {
    data = 'bar'
    lastBlock = Block.genesis()
    block = Block.mineBlock(lastBlock, data)
  })

  it('set data match input', () => {
    expect(block.data).toEqual('bar')
  }, 2000)

  it('set the "lastHash" match the input', () => {
    expect(block.lastHashValue).toEqual(lastBlock.hashValue)
  })
})
