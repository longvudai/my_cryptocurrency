import BlockChain from '../src/blockchain'
import Block from '../src/blockchain/Block'

describe('BlockChain', () => {
  let blockChain

  beforeEach(() => {
    blockChain = new BlockChain()
  })

  it('check genesis block', () => {
    expect(blockChain.chain[0].data).toEqual(Block.genesis().data)
  })

  it('add new Block', () => {
    let data = 'bar'
    blockChain.addBlock(data)
    expect(blockChain.chain[blockChain.chain.length - 1].lastHashValue).toEqual(
      Block.genesis().hashValue
    )
  })

  it('validate the chain', () => {
    let lastBlock = blockChain.lastBlock()
    lastBlock.data = 'foo'
    expect(blockChain.isValidChain(blockChain.chain)).toBe(false)
  })

  it('replace the chain', () => {
    let newBlockchain = new BlockChain()
    newBlockchain.addBlock('foo')

    newBlockchain.addBlock('bar')

    blockChain.replaceTheChain(newBlockchain.chain)

    expect(blockChain.chain.length).toEqual(newBlockchain.chain.length)
  })
})
