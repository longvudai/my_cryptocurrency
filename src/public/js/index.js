function getBlockInfo() {
  console.log('GET')
  $.get('/blocks').then(data => {
    let $parent = $('#chain')
    console.log($parent)
    $parent.empty()
    $parent.html(renderjson(data))
  })
}

function getMinerInfo() {
  $.get('/miner').then(data => {
    $('#miner-addr').text(data.address)
    $('#miner-balance').text(data.balance)
  })
}

function getWalletInfo() {
  $.get('/wallet').then(data => {
    $('#wallet-addr').text(data.address)
    $('#wallet-balance').text(data.balance)
  })
}

function inspectTransactions() {
  let $trans = $('.trans-list')
  $trans.empty()
  $('#mine-result').empty()
  $.get('/pool').then(data => {
    $trans.html(renderjson(data.transaction))
  })
}

function mine() {
  let $loader = $('#loader')
  $loader.removeClass('inactive')
  $loader.addClass('active')
  $.get('/mining', function(data) {
    if (data.status == 200) {
      $('#mine-result').html(`<p>Thành công</p><p>Số tiền thưởng: 50</p>`)
      $loader.addClass('inactive')
      $loader.removeClass('active')
      getMinerInfo()
      getBlockInfo()
      inspectTransactions()
    } else {
      $('#mine-result').text('Thất bại')
    }
  })
}

$('#transfer > button').click(function(event) {
  event.preventDefault()
  let recipient = $('#transfer-recipient').val()
  let amount = parseInt($('#transfer-amount').val())
  if (!recipient || amount <= 0) {
    alert('Transfer information is invalid')
  } else {
    let postData = {
      txType: 'TRANSFER',
      recipient: {
        publicKey: recipient
      },
      data: amount
    }
    $.ajax({
      url: '/transaction',
      type: 'POST',
      dataType: 'json',
      data: JSON.stringify(postData),
      contentType: 'application/json;charset=UTF-8'
    }).then(data => {
      if (data.error) {
        $('.transfer-money .send-result').text(JSON.stringify(data))
      } else {
        $('.transfer-money .send-result').text(
          'Gửi giao dịch lên hệ thống thành công'
        )
      }
    })
  }
})

$('#contract > button').click(function(event) {
  event.preventDefault()
  let postData = {
    txType: 'SMART_CONTRACT',
    data: {
      senderAmount: parseInt($('#send-amount').val()),
      recipientAmount: parseInt($('#interest-amount').val())
    },
    recipient: {
      publicKey: $('#second-party').val(),
      balance: parseInt($('#balance').val())
    },
    endTime: parseInt($('#expire-time').val())
  }
  console.log(postData)
  $.ajax({
    url: '/transaction',
    type: 'POST',
    dataType: 'json',
    data: JSON.stringify(postData),
    contentType: 'application/json;charset=UTF-8'
  }).then(data => {
    if (data.error) {
      $('#contract-result').text(data.error)
    } else {
      $('#contract-result').text('Gửi hợp đồng lên hệ thống thành công')
    }
  })
})

function setBackgroundColor() {
  let urlPort = parseInt(window.location.port || '80')
  let colors = ['#fff', '#7FDBFF', '#01FF70', '#AAAAAA', '#DDDDDD']
  console.log(urlPort, urlPort % colors.length)
  $(document.body).css('background-color', colors[urlPort % colors.length])
}

function CopyToClipboard(containerid) {
  if (document.selection) {
    var range = document.body.createTextRange()
    range.moveToElementText(document.getElementById(containerid))
    range.select().createTextRange()
    document.execCommand('copy')
  } else if (window.getSelection) {
    var range = document.createRange()
    range.selectNode(document.getElementById(containerid))
    window.getSelection().removeAllRanges()
    window.getSelection().addRange(range)
    document.execCommand('copy')
  }
  alert('Thành công')
}

$(document).ready(function() {
  setBackgroundColor()
  getMinerInfo()
  getWalletInfo()
  getBlockInfo()
})
