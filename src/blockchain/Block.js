import SHA256 from 'crypto-js/sha256'
import { formatDate } from '../utils/Datetime'
import {
  MAX_TARGET,
  MINING_TIME,
  getDifficulity,
  adjustDifficulity
} from '../utils/Constant'
import { MY_WALLET } from '../wallet'
import Transaction from '../transaction/Transaction'

class Block {
  constructor(timestamp, lastHashValue, hashValue, data, miningTime, nonce) {
    this.hashValue = hashValue
    this.timestamp = timestamp
    this.lastHashValue = lastHashValue
    this.data = data
    this.nonce = nonce
    this.difficulity = getDifficulity()
    this.target = MAX_TARGET / this.difficulity
    this.miningTime = miningTime
  }

  static genesis = () => {
    let timestamp = 1575286923302 // 2019-12-02
    // const txData = {
    //   txType: 'NORMAL',
    //   data: 21000
    // }
    // let initialTx = new Transaction(null, MY_WALLET.publicKey, txData)
    const data = []
    const hashValue = this.hash(timestamp, 'none last hash', data, 0)
    return new this(timestamp, '123456789', hashValue, data, MINING_TIME, 0)
  }

  // Gia su nang luc dao la khong doi
  static mineBlock = (lastBlock, data) => {
    let timestamp = Date.now()
    const lastHashValue = lastBlock.hashValue

    let startTime = new Date().getTime()
    const target = MAX_TARGET / getDifficulity()
    let nonce = 0
    let hashValue = this.hash(timestamp, lastHashValue, data, nonce)
    let hashNum = MAX_TARGET

    const endTime = new Date().getTime()
    let miningTime = endTime - startTime
    do {
      nonce += 1
      timestamp = Date.now()
      hashValue = this.hash(timestamp, lastHashValue, data, nonce)
      hashNum = parseInt(hashValue.slice(0, 32), 16)

      miningTime = new Date().getTime() - startTime
    } while (hashNum > target || miningTime > 15000)

    if (hashNum > target) {
      return
    }
    // const endTime = new Date().getTime()
    // const miningTime = endTime - startTime

    // console.log('miningTime', miningTime)

    const newBlock = new this(
      timestamp,
      lastHashValue,
      hashValue,
      data,
      miningTime,
      nonce
    )

    // adjust difficulity for next time
    adjustDifficulity(miningTime)

    return newBlock
  }

  static hash = (timestamp, lastHashValue, data, nonce) => {
    // console.log(`
    // HASHING.....
    // Block -
    // ${timestamp}
    // ${lastHashValue}
    // ${data}
    // ${nonce}
    // .......
    // `)
    return SHA256(`${timestamp}${lastHashValue}${data}${nonce}`).toString()
  }

  static isValidBlock = block => {
    const { timestamp, lastHashValue, data, nonce } = block
    console.log(`
    Block -
    ${timestamp}
    ${lastHashValue}
    ${data}
    ${nonce}

    `)

    console.log(JSON.stringify(block))
    const hashValue = this.hash(timestamp, lastHashValue, data, nonce)
    if (hashValue !== block.hashValue) {
      console.log(hashValue)
      console.log(block.hashValue)
      console.log('in valid block')
    }
    return hashValue === block.hashValue
  }

  description = () => {
    return `
    Block -
    ${formatDate(this.timestamp)}
    ${this.lashHash}
    ${this.hash}
    ${this.data}
    ${this.nonce}
    ${this.difficulity}
    ${this.target}
    ${this.miningTime}
    `
  }
}

export default Block
