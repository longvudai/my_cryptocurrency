import uuidV1 from 'uuid/v1'
import SHA256 from 'crypto-js/sha256'

const TRANSACTION_STATE = {
  CONFIRM: 'CONFIRM',
  UNCONFIRM: 'UNCONFIRM'
}

const TRANSACTION_TYPE = {
  SMART_CONTRACT: 'SMART_CONTRACT',
  TRANSFER: 'TRANSFER'
}
export { TRANSACTION_TYPE }
class Transaction {
  constructor(sender, recipient, content) {
    this.id = uuidV1()
    this.sender = sender
    this.recipient = recipient
    this.content = content
    this.state = TRANSACTION_STATE.UNCONFIRM
  }

  hash = () => {
    return SHA256(
      `${this.id}${this.sender}${this.recipient}${this.content}`
    ).toString()
  }

  changeState = (state = TRANSACTION_STATE.CONFIRM) => {
    this.state = state
  }

  static decodeTransaction(tranObj) {
    let { id, sender, recipient, content, state, signature } = tranObj
    let tran = new Transaction(sender, recipient, content)
    tran.id = id
    tran.state = state
    tran.signature = signature
    return tran
  }
}

export default Transaction
