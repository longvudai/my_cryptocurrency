const toHexString = data => {
  const buf = new Buffer.from(data)
  return buf.toString('hex')
}

const getBuffer = str => {
  const buf = new Buffer.from(str, 'hex')
  return buf
}

export { toHexString, getBuffer }
