import Elliptic from 'elliptic'
const ellipticCurve = Elliptic.ec

const ec = new ellipticCurve('secp256k1')

const genKeyPair = () => {
  return ec.genKeyPair()
}

const keyFromPublic = publicKey => {
  return ec.keyFromPublic(publicKey, 'hex')
}

export { genKeyPair, keyFromPublic }
