const target = '0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF'.slice(
  0,
  32
)

const MINING_TIME = 2000
let DIFFICULITY = 800
const getDifficulity = () => {
  return DIFFICULITY
}

const adjustDifficulity = actualTime => {
  DIFFICULITY = (DIFFICULITY * MINING_TIME) / actualTime
}

const MAX_TARGET = parseInt(target)

export { MAX_TARGET, getDifficulity, adjustDifficulity, MINING_TIME }
