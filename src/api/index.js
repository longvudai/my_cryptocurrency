import express from 'express'
import bodyParser from 'body-parser'
import path from 'path'
import BlockChain from '../blockchain'
import P2PServer from './P2PServer'

import Wallet from '../wallet'
import TransactionPool from '../transaction/TransactionPool'
import { TRANSACTION_TYPE } from '../transaction/Transaction'
import Miner from './Miner'

const HTTP_PORT = process.env.HTTP_PORT || 3001

const app = express()
const publicRoot = path.resolve(path.join(__dirname, '..', 'public'))

const wallet = new Wallet()

const blockChain = new BlockChain()
const pool = new TransactionPool()

const p2pServer = new P2PServer(blockChain, pool)

const miner = new Miner(blockChain, pool, p2pServer)

app.use(bodyParser.json())
app.use(express.static(publicRoot))

app.get('/', (req, res) => res.sendFile('index.html', { root: publicRoot }))

app.get('/address', (req, res) =>
  res.json({
    address: wallet.publicKey
  })
)

app.get('/blocks', (req, res) => {
  res.json(blockChain.chain)
})

app.get('/wallet', (req, res) => {
  res.json({
    address: wallet.publicKey,
    balance: wallet.getBalance(blockChain.chain)
  })
})

app.get('/miner', (req, res) => {
  res.json({
    address: miner.getAddress(),
    balance: miner.getBalance()
  })
})

app.get('/mining', (req, res) => {
  miner.mine()

  res.json({
    status: '200'
  })
})

app.get('/pool', (req, res) =>
  res.json({
    transaction: pool.transactions
  })
)

app.get('/smartcontract', (req, res) => {
  res.json({
    smartContract: wallet.getSmartContracts(blockChain.chain)
  })
})

app.post('/transaction', (req, res) => {
  const { txType, data, recipient, endTime } = req.body
  let txData = {}
  switch (txType) {
    case TRANSACTION_TYPE.TRANSFER:
      txData = {
        txType,
        data
      }
      break
    case TRANSACTION_TYPE.SMART_CONTRACT:
      const startTime = Date.now()
      txData = {
        txType,
        data: {
          ...data,
          startTime,
          endTime: startTime + endTime,
          state: 'STARTED'
        }
      }
      break
    default:
      res.json({
        error: 'Transaction type is unknown'
      })
  }

  let transaction = wallet.createTransaction(
    txData,
    recipient,
    blockChain.chain
  )
  if (!transaction) {
    res.json({
      error: 'can not create transaction'
    })
    return
  }
  pool.submit(transaction)
  p2pServer.broadcastPool()
  res.redirect('/pool')
})

const listen = () => {
  app.listen(HTTP_PORT, () =>
    console.log(`server listening on port ${HTTP_PORT}!`)
  )
  p2pServer.listen()
}

listen()
