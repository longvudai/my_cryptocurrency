import Websocket from 'ws'
import Transaction from '../transaction/Transaction'
// import { MY_WALLET } from '../wallet'

const P2P_PORT = process.env.P2P_PORT || 5001

const peers = process.env.PEERS ? process.env.PEERS.split(',') : []

const MESSAGE_TYPE = {
  chain: 'CHAIN',
  pool: 'POOL'
}

class P2PServer {
  constructor(blockChain, pool) {
    this.blockChain = blockChain
    this.pool = pool
    this.sockets = []
  }

  connectSocket = socket => {
    this.sockets.push(socket)

    this.closeConnectionHandle(socket)
    this.messageHandle(socket)

    this.synchronizeChain(socket)

    console.log(`socket was connected`)
  }

  connectNeighborPeers = () => {
    peers.forEach(peerAddr => {
      const socket = new Websocket(peerAddr)
      socket.on('open', () => this.connectSocket(socket))
    })
  }
  closeConnectionHandle = socket => {
    socket.on('close', () => {
      this.sockets = this.sockets.filter(
        e => JSON.stringify(e) !== JSON.stringify(socket)
      )
      console.log('sockets:', this.sockets.length)
      console.log('Connection was closed')
    })
  }

  messageHandle = socket => {
    socket.on('message', message => {
      const payload = JSON.parse(message)

      switch (payload.type) {
        case MESSAGE_TYPE.chain:
          console.log('✅ Sync the chain')
          this.blockChain.replaceTheChain(payload.data)
          break
        case MESSAGE_TYPE.pool:
          console.log('✅ Sync the pool')
          // this.pool.replacePool(this.blockChain.chain, payload.data)
          console.log(payload.data)
          this.pool.transactions = payload.data.map(transaction =>
            Transaction.decodeTransaction(transaction)
          )
        //   this.pool.transactions = payload.data
      }
    })
  }

  synchronizeChain = socket => {
    socket.send(
      JSON.stringify({
        type: MESSAGE_TYPE.chain,
        data: this.blockChain.chain
      })
    )
  }

  synchronizePool = socket => {
    socket.send(
      JSON.stringify({
        type: MESSAGE_TYPE.pool,
        data: this.pool.transactions
      })
    )
  }

  broadcastChain = () => {
    console.log('🔄 Boardcasting the chain...')
    this.sockets.forEach(socket => this.synchronizeChain(socket))
  }

  broadcastPool = () => {
    console.log('🔄 Boardcasting the pool...')
    this.sockets.forEach(socket => this.synchronizePool(socket))
  }

  // boardcastClearPool = () => {
  //   console.log('🔄 Boardcasting the clear pool...')
  //   this.sockets.forEach
  // }

  listen = () => {
    const node = new Websocket.Server({ port: P2P_PORT })

    // listen for another peer connect to
    node.on('connection', socket => {
      this.connectSocket(socket)
    })

    // connect to neighbor peers
    this.connectNeighborPeers()

    console.log(`Listen for peer-to-peer connection on: ${P2P_PORT}`)
  }
}

export default P2PServer
