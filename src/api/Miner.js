import Wallet from '../wallet'
class Miner {
  constructor(blockChain, pool, p2pServer) {
    this.blockChain = blockChain
    this.pool = pool
    this.p2pServer = p2pServer
    this.wallet = new Wallet()
  }

  mine = () => {
    // pull transaction
    const transactions = this.pool.pull()
    if (transactions.length == 0) return

    // mining to create block
    let block = this.blockChain.addBlock(transactions)

    // failed
    if (!block) {
      return
    }

    // include reward
    const reward = 50
    block.reward = reward
    block.minedBy = this.wallet.publicKey

    // broadcast clear pool
    this.pool.clear()
    this.p2pServer.broadcastPool()

    // broadcast chain
    this.p2pServer.broadcastChain()
  }

  getAddress = () => {
    return this.wallet.publicKey
  }

  getBalance = () => {
    // show balance
    const balance = this.wallet.getBalance(this.blockChain.chain)
    // console.log('BALANCE of MINER:', balance)
    return balance
  }
}

export default Miner
